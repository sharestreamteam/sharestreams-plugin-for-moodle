<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
// The following is the ShareStream's Plugin for Moodle and is licensed pursuant to GNU Public License version 3.0.
/**
 * The ShareStream Pick-n-Play block class
 *
 * @package    block_pnp_next
 * @copyright  2017 ShareStream. All rights reserved.
 */
defined('MOODLE_INTERNAL') || die;
header('P3P: CP="CAO PSA OUR"');
global $CFG;
require_once($CFG->dirroot . '/local/sharestream/lib.php');
require_once($CFG->dirroot . '/local/sharestream/lib/sharestream_client.class.php');

class block_pnp_next extends block_base {
    public function init() {
        $this->blockname = get_class( $this );
        $this->title = 'ShareStream Pick-n-Play (new)';
    }

    public function has_config() {
        return true;
    }

    public function applicable_formats() {
        return array(
            'course-view' => true,
            'course-view-social' => false
        );
    }

    public function get_content() {
        global $CFG, $COURSE, $USER;

        if (isset($this->content)) {
            return $this->content;
        }

        $allowedroles = explode(',', get_config('moodle', 'block_pnp_next_roleselection'));

        $context = context_course::instance($COURSE->id);
        $courseroles = get_user_roles($context, $USER->id);

        $displaycontent = false;
        foreach ($courseroles as $courserole) {
            if (in_array($courserole->roleid, $allowedroles)) {
                $displaycontent = true;
            }
        }

        if ($displaycontent) {
            $courseid = $COURSE->id;
            $webroot = $CFG->wwwroot;

            $config = new sharestream_config();
            $client = new sharestream_client();
            $ltipickerparams = $client->get_lti_pnp_next_params();

            $this->content = new stdClass;
            $this->content->text = '<br><a href="' . $webroot . '/blocks/pnp_next/p.php?id=' . $courseid;
            $this->content->text = $this->content->text . '&permlink=' . urlencode($ltipickerparams);
            $this->content->text = $this->content->text . '">ShareStream Pick-n-Play</a>';
        }

        return $this->content;
    }
}