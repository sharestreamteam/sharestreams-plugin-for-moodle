<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
// The following is the ShareStream's Plugin for Moodle and is licensed pursuant to GNU Public License version 3.0.

/**
 * Block version details
 *
 * @package    block_pnp_next
 * @copyright  2017 ShareStream. All rights reserved.
 */
defined('MOODLE_INTERNAL') || die;

global $DB;

$roles = $DB->get_records('role', null, 'sortorder ASC');
$defaultroles = array('editingteacher', 'teacher');
$defaults = array_filter($roles, function ($role) use ($defaultroles) {
    return in_array($role->shortname, $defaultroles);
});
$onlynames = function ($role) {
    return $role->name ? trim(format_string($role->name)) : $role->shortname;
};

$settings->add(
    new admin_setting_configmultiselect('block_pnp_next_roleselection',
        get_string('selectroles', 'block_pnp_next'),
        get_string('selectroles', 'block_pnp_next'),
        array_keys($defaults),
        array_map($onlynames, $roles)
    )
);