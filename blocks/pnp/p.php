<?php
/**
 * The ShareStream Pick-n-Play page.
 */
header('P3P: CP="CAO PSA OUR"');
require_once('../../config.php');

// Check for the id parameter. If the parameter doesn't exist, then an error is
// thrown.
$course_id = required_param( 'id', PARAM_INT );
$perm_link = required_param( 'permlink', PARAM_URL );
$PAGE->set_url( '/blocks/pnp/p.php', array('id' => $course_id) );

// Make sure the course is valid.
$course = $DB->get_record( 'course', array('id' => $course_id) );
if ( !$course ) {
    print_error( 'invalidcourseid' );
}

require_login( $course_id );
$PAGE->set_pagelayout( 'incourse' );

// Set the header and titles
$pagename = 'ShareStream Pick-n-Play';
$PAGE->navbar->add( $pagename );
$PAGE->set_title( $pagename );
$PAGE->set_button( $pagename );
$PAGE->set_heading( $pagename );

// Output the HTML
echo $OUTPUT->header();
echo '<iframe id="PnPFrame" width="98%" height="800px" src="' . $perm_link . '" allowfullscreen frameborder="0"></iframe>';
echo $OUTPUT->footer();