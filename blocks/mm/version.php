<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
// The following is the ShareStream's Plugin for Moodle and is licensed pursuant to GNU Public License version 3.0.

/**
 * Block version details
 *
 * @package    block_mm
 * @copyright  2017 ShareStream. All rights reserved.
 */
defined('MOODLE_INTERNAL') || die('MOODLE_INTERNAL Undefined');
$plugin = new stdClass();
$plugin->component = 'block_mm';
$plugin->version = 2020021000;
$plugin->requires = 2012062500;
$plugin->release = '1.0.0';
$plugin->maturity = MATURITY_STABLE;
$plugin->dependencies = array('mod_lti' => 2011112900);
$plugin->cron = 0;