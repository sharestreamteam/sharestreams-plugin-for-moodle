<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
defined('MOODLE_INTERNAL') || die('Invalid access');
global $CFG;
require_once($CFG->dirroot. '/mod/lti/locallib.php');

class filter_sharestream extends moodle_text_filter {
    public function filter($text, array $options = array()) {
        global $COURSE, $USER;

        if (preg_match_all('|\[sharestream:(.*)\]|U', $text, $matches_code)) {
            foreach ($matches_code[0] as $ci => $code) {
                $textToReplace = $code;
                $assetId = str_replace("[sharestream:", "", $code);
                $assetId = str_replace("]", "", $assetId);

                $ltiPlayerParams = $this->get_lti_player_params_with_user($assetId, $COURSE->id, $USER->id, $USER->username);
                $uniqueId = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
                $iFrameSrc = '<iframe id="' . $uniqueId . '" src="' . $ltiPlayerParams . '" width="640px" height="362px" allowfullscreen></iframe>';
                $text = str_replace($textToReplace, $iFrameSrc, $text);
            }
        }

        return $text;
    }

    public function get_lti_player_params_with_user($assetid, $courseid, $userid, $username) {
        global $DB;
        $course = $DB->get_record('course', array('id' => (int)$courseid), '*', MUST_EXIST);
        $user = $DB->get_record('user', array('id' => (int)$userid), '*', MUST_EXIST);
        $ltiparams = $this->get_lti_params_with_user($course, $userid, $username, $user);
        $ltiparams['id'] = $assetid;
        $sharestream_url = get_config('local_sharestream', 'sharestream_lti');
        $params = $this->get_signed_lti_params(parse_url($sharestream_url)['scheme'] . '://' . parse_url($sharestream_url)['host'] . '/mediacloud/lti/embed/play',
            'GET',
            $course->id,
            $ltiparams);

        return parse_url($sharestream_url)['scheme'] . '://' . parse_url($sharestream_url)['host'] .
            '/mediacloud/lti/embed/play' . '?' . http_build_query($params, '', '&');
    }

    public function get_lti_params_with_user($course, $userid, $username, $user) {
        global $CFG;
        $use_shortname = get_config('local_sharestream', 'sharestream_use_shortname');
        $usergiven = (isset($user->firstname)) ? $user->firstname : '';
        $userfamily = (isset($user->lastname)) ? $user->lastname : '';
        $userfull = trim($usergiven . ' ' . $userfamily);
        $useremail = (isset($user->email)) ? $user->email : '';
        if (floatval($CFG->release) < 2.8) {
            $roles = lti_get_ims_role($user, 0, $course->id);
        } else {
            $roles = lti_get_ims_role($user, 0, $course->id, false);
        }
        $params = array(
            'context_id' => $course->id,
            'context_label' => $course->shortname,
            'context_title' => $course->fullname,
            'ext_lms' => 'moodle-2',
            'lis_person_name_family' => $userfamily,
            'lis_person_name_full' => $userfull,
            'lis_person_name_given' => $usergiven,
            'lis_person_contact_email_primary' => $useremail,
            'lti_message_type' => 'basic-lti-launch-request',
            'lti_version' => 'LTI-1p0',
            'roles' => $roles,
            'tool_consumer_info_product_family_code' => 'moodle',
            'tool_consumer_info_version' => (string)$CFG->version,
            'user_id' => $userid,
            'custom_context_id' => $course->idnumber,
            'custom_moodle_course_id' => ($use_shortname ? $course->shortname : $course->idnumber),
            'lis_person_sourcedid' => $username,
            'javascriptCallback' => 'function loadLMSJS(assetId){window.parent.postMessage({"assetId":assetId}, "*");}'
        );
        $params['custom_moodle_user_login_id'] = $username;
        if (isset($course->idnumber)) {
            $params['context_type'] = 'CourseSection';
            $params['resource_link_id'] = $course->idnumber;
            $params['lis_course_section_sourcedid'] = $course->idnumber;
            $params['lmsCourseId'] = $course->idnumber;
        }

        return $params;
    }

    public function get_signed_lti_params($endpoint, $method='GET',
                                          $courseid=null, $params=array()) {
        global $DB;

        $course = $DB->get_record('course', array('id' => (int)$courseid), '*', MUST_EXIST);
        $key = get_config('local_sharestream', 'sharestream_key');
        $secret = get_config('local_sharestream', 'sharestream_secret');
        $queryparams = $this->get_lti_params($course);
        return lti_sign_parameters(array_replace($queryparams, $params),
            $endpoint, $method, $key, $secret);
    }

    public function get_lti_params($course) {
        global $USER, $CFG;
        $use_shortname = get_config('local_sharestream', 'sharestream_use_shortname');
        $usergiven = (isset($USER->firstname)) ? $USER->firstname : '';
        $userfamily = (isset($USER->lastname)) ? $USER->lastname : '';
        $userfull = trim($usergiven . ' ' . $userfamily);
        $useremail = (isset($USER->email)) ? $USER->email : '';
        if (floatval($CFG->release) < 2.8) {
            $roles = lti_get_ims_role($USER, 0, $course->id);
        } else {
            $roles = lti_get_ims_role($USER, 0, $course->id, false);
        }
        $params = array(
            'context_id' => $course->id,
            'context_label' => $course->shortname,
            'context_title' => $course->fullname,
            'ext_lms' => 'moodle-2',
            'lis_person_name_family' => $userfamily,
            'lis_person_name_full' => $userfull,
            'lis_person_name_given' => $usergiven,
            'lis_person_contact_email_primary' => $useremail,
            'lti_message_type' => 'basic-lti-launch-request',
            'lti_version' => 'LTI-1p0',
            'roles' => $roles,
            'tool_consumer_info_product_family_code' => 'moodle',
            'tool_consumer_info_version' => (string)$CFG->version,
            'user_id' => $USER->id,
            'custom_context_id' => $course->idnumber,
            'custom_moodle_course_id' => ($use_shortname ? $course->shortname : $course->idnumber),
            'lis_person_sourcedid' => $USER->username,
            'javascriptCallback' => 'function loadLMSJS(assetId){window.parent.postMessage({"assetId":assetId}, "*");}'
        );
        if (isset($USER->username)) {
            $params['custom_moodle_user_login_id'] = $USER->username;
        }
        if (isset($course->idnumber)) {
            $params['context_type'] = 'CourseSection';
            $params['resource_link_id'] = $course->idnumber;
            $params['lis_course_section_sourcedid'] = $course->idnumber;
            $params['lmsCourseId'] = $course->idnumber;
        }

        return $params;
    }
}