// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


Y.namespace('M.atto_sharestream').Button = Y.Base.create('button', Y.M.editor_atto.EditorPlugin, [], {
    /**
     * Add event listeners.
     *
     * @method initializer
     */
	initializer: function() {
		this.addButton({
			icon: 'e/icon',
			iconComponent: 'atto_sharestream',
			callback: this._handlesharestream
		});
    },

	addEvt: function(el, evt, fn, bubble) {
		if ('addEventListener' in el) {
			try {
				el.addEventListener(evt, fn, bubble);
			} catch(e) {
				if (typeof fn == 'object' && fn.handleEvent) {
					el.addEventListener(evt, function(e){
						fn.handleEvent.call(fn,e);
					}, bubble);
				} else {
					throw e;
				}
			}
		} else if ('attachEvent' in el) {
			if (typeof fn == 'object' && fn.handleEvent) {
				el.attachEvent('on' + evt, function(){
					// Bind fn as this
					fn.handleEvent.call(fn);
				});
			} else {
				el.attachEvent('on' + evt, fn);
			}
		}
	},

	GetURLParameter: function(sParam, sPageURL) {
		if(typeof sPageURL == 'undefined')
			return(null);

		sPageURL = sPageURL.substring(sPageURL.indexOf("?")+1);

		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) {
				return sParameterName[1];
			}
		}

		return(null);
	},

	_handlesharestream : function() {
		var self = this;

		self.editor = null;

		var child = window.open(self.get('sharestream_url'), '_ss','width=400, height=500');

		var leftDomain = false;
		var interval = setInterval(function() {
			try {
				if (child.document.domain === document.domain) {
					if (leftDomain && child.document.readyState === "complete") {
						clearInterval(interval);
						var redirectUri = child.window.location.href;
						child.close();

						var assetId = '';
						var assetTitle = '';

						redirectUri = redirectUri.substring(redirectUri.indexOf("?")+1);
						var uriParameters = redirectUri.split('&');
						for (var i = 0; i < uriParameters.length; i++) {
							var parameterName = uriParameters[i].split('=');
							if (parameterName[0] == 'assetId') {
								assetId = parameterName[1];
							} else if (parameterName[0] == 'assetTitle') {
								assetTitle = parameterName[1];
							}
						}

						try {
							self.get('host').insertContentAtFocusPoint('[sharestream:' + assetId + ']');
						} catch(e) {
							alert(e);
						}
					}
				} else {
					leftDomain = true;
				}
			} catch(e) {
				if (child.closed) {
					clearInterval(interval);
					return; 
				}
				leftDomain = true;
			}

		}, 500);

		return(true);
	}
}, {
    ATTRS: {
		sharestream_url: {
			value: '<defaultvalue>'
		}
    }
});