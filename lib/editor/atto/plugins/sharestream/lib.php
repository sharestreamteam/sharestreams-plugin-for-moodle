<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die('Invalid access');
require_once($CFG->dirroot. '/mod/lti/locallib.php');

global $CFG, $COURSE;

function atto_sharestream_params_for_js() {
	global $CFG, $COURSE;

    $sharestream_url = get_config('local_sharestream', 'sharestream_lti');


    $params = get_signed_lti_params($sharestream_url,
        'GET',
        $COURSE->id,
        get_lti_params($COURSE));
    $url = $sharestream_url. '?' . http_build_query($params, '', '&');

//    $url = $sharestream_url . "&key=" . $sharestream_key . "&secret=" . $sharestream_secret;

	return(array(
		'sharestream_url' => $url
	));
}

function get_lti_params($course) {
    global $USER, $CFG;

    $usergiven = (isset($USER->firstname)) ? $USER->firstname : '';
    $userfamily = (isset($USER->lastname)) ? $USER->lastname : '';
    $userfull = trim($usergiven . ' ' . $userfamily);
    $useremail = (isset($USER->email)) ? $USER->email : '';
    if (floatval($CFG->release) < 2.8) {
        $roles = lti_get_ims_role($USER, 0, $course->id);
    } else {
        $roles = lti_get_ims_role($USER, 0, $course->id, false);
    }
    $params = array(
        'context_id' => $course->id,
        'context_label' => $course->shortname,
        'context_title' => $course->fullname,
        'ext_lms' => 'moodle-2',
        'lis_person_name_family' => $userfamily,
        'lis_person_name_full' => $userfull,
        'lis_person_name_given' => $usergiven,
        'lis_person_contact_email_primary' => $useremail,
        'lti_message_type' => 'basic-lti-launch-request',
        'lti_version' => 'LTI-1p0',
        'roles' => $roles,
        'tool_consumer_info_product_family_code' => 'moodle',
        'tool_consumer_info_version' => (string)$CFG->version,
        'user_id' => $USER->id,
        'custom_context_id' => $course->idnumber,
        'custom_moodle_course_id' => $course->shortname,
        'lis_person_sourcedid' => $USER->username,
        'javascriptCallback' => 'function loadLMSJS(assetId){window.parent.postMessage({"assetId":assetId}, "*");}',
        'custom_moodle_return_url' => $CFG->wwwroot
    );
    if (isset($USER->username)) {
        $params['custom_moodle_user_login_id'] = $USER->username;
    }
    if (isset($course->idnumber)) {
        $params['context_type'] = 'CourseSection';
        $params['resource_link_id'] = $course->idnumber;
        $params['lis_course_section_sourcedid'] = $course->idnumber;
        $params['lmsCourseId'] = $course->idnumber;
    }

    return $params;
}

function get_signed_lti_params($endpoint, $method='GET',
                                      $courseid=null, $params=array()) {
    global $DB;

    $course = $DB->get_record('course', array('id' => (int)$courseid), '*', MUST_EXIST);
    $key = get_config('local_sharestream', 'sharestream_key');
    $secret = get_config('local_sharestream', 'sharestream_secret');
    $queryparams = get_lti_params($course);
    return lti_sign_parameters(array_replace($queryparams, $params),
        $endpoint, $method, $key, $secret);
}