<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

function xmldb_atto_sharestream_install() {
    global $CFG;

    $toolbar = get_config('editor_atto', 'toolbar');
    $pos = stristr($toolbar, 'sharestream');

    if (!$pos) {
        // Add imagedragdrop after image plugin.
        $toolbar = preg_replace('/(.+?=.+?)media($|\s|,)/m', '$1media, sharestream$2', $toolbar, 1);
		set_config('toolbar', $toolbar, 'editor_atto');
    }

    return true;
}
