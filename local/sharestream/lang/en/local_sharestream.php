<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// Plugin settings.
$string['pluginname'] = 'sharestream Plugin Configuration';

$string['setting_heading_desc'] = 'These settings customize the method in which your Moodle instance connects to sharestream.<br/><br/>You may need to purge your Moodle caches after changing these settings for them to take effect.<br/><br/>';

$string['setting_lti_label'] = 'Your sharestream LTI Launch URL';
$string['setting_lti_desc'] = 'The LTI launch URL of your sharestream site - for example, "https://example.sharestream.com/api/lti/".  Make sure you include the "https://", as well as the trailing slash.<br/><br/>';

$string['setting_key_label'] = 'Your sharestream Consumer Key';
$string['setting_key_desc'] = 'The provided consumer key for your sharestream site.<br/><br/>';

$string['setting_secret_label'] = 'Your sharestream Consumer Secret';
$string['setting_secret_desc'] = 'The provided consumer secret for your sharestream site.<br/><br/>';

$string['setting_use_shortname_label'] = 'Use Short Name as Course ID';
$string['setting_use_shortname_desc'] = 'Use Short Name as Course ID.<br/><br/>';