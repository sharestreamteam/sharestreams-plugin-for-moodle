<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// The following is the ShareStream's Plugin for Moodle and is licensed pursuant to GNU Public License version 3.0.

defined('MOODLE_INTERNAL') || die('MOODLE_INTERNAL Undefined');

class sharestream_config
{
    public $host;
    public $protocol;
    public $consumerkey;
    public $sharedsecret;
    public $moodletoken;
    public $useltiauth;
    public $useshortname;
    public $wwwroot;

    public function __construct() {
        global $CFG, $DB;

        $this->wwwroot = $CFG->wwwroot;

        $records = $DB->get_records('config_plugins', array('plugin' => 'local_sharestream'));
        if (!empty($records)) {
            foreach ($records as $r) {
                switch($r->name) {
                    case 'sharestream_lti':
                        $this->protocol = parse_url($r->value, PHP_URL_SCHEME);
                        $this->host = parse_url($r->value, PHP_URL_HOST);
                        break;
                    case 'sharestream_key':
                        $this->consumerkey = $r->value;
                        break;
                    case 'sharestream_secret':
                        $this->sharedsecret = $r->value;
                        break;
                    case 'sharestream_use_shortname':
                        $this->useshortname = $r->value;
                    default:
                }
            }
        }
    }
}
